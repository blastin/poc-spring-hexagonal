package br.projeto.demo.aplicacao.livro;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class FormatarData {

    static final String PADRAO_DATA = "dd/MM/yyyy";

    public static LocalDate localDate(String dataLancamento) {
        return LocalDate.parse(dataLancamento, DateTimeFormatter.ofPattern(PADRAO_DATA));
    }

    public static String formatarData(final LocalDate localDate) {
        return localDate.format(DateTimeFormatter.ofPattern(PADRAO_DATA));
    }

    private FormatarData() {
    }
}
