package br.projeto.demo.aplicacao.livro.portas;

import br.projeto.demo.aplicacao.livro.Livro;

import java.util.Set;

@FunctionalInterface
public interface CasoDeUsoCatalogarLivrosRecentes {

    Set<Livro> catalogar();

}
