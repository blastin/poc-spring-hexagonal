package br.projeto.demo.aplicacao.livro;

import br.projeto.demo.aplicacao.livro.portas.CasoDeUsoCatalogarLivrosRecentes;
import br.projeto.demo.aplicacao.livro.portas.PortaRecuperarLivros;

import java.time.LocalDate;
import java.util.Set;
import java.util.stream.Collectors;

public final class CatalogarLivrosRecentes implements CasoDeUsoCatalogarLivrosRecentes {

    public CatalogarLivrosRecentes(final PortaRecuperarLivros portaRecuperarLivros) {
        this.portaRecuperarLivros = portaRecuperarLivros;
    }

    private final PortaRecuperarLivros portaRecuperarLivros;

    @Override
    public Set<Livro> catalogar() {

        final LocalDate data = LocalDate.now();

        return portaRecuperarLivros
                .recuperar()
                .stream()
                .filter(livro -> livro.anoAtual(data))
                .collect(Collectors.toUnmodifiableSet());

    }

}
