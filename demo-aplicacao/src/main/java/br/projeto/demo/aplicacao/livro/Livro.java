package br.projeto.demo.aplicacao.livro;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

public final class Livro {

    public static LivroConstrutor builder() {
        return new LivroConstrutor();
    }

    public static class LivroConstrutor {

        private LivroConstrutor() {
        }

        private long id;

        private String nome;

        private LocalDate dataLancamento;

        private BigDecimal custo;

        public LivroConstrutor comId(final long id) {
            if (id < 0) throw new IllegalArgumentException("Id deve ser um valor do tipo long maior que zero");
            this.id = id;
            return this;
        }

        public LivroConstrutor seuNome(final String nome) {
            Objects.requireNonNull(nome);
            this.nome = nome.strip();
            return this;
        }

        public LivroConstrutor lancamentoOcorreu(final String dataLancamento) {
            Objects.requireNonNull(dataLancamento);
            this.dataLancamento = FormatarData.localDate(dataLancamento);
            return this;
        }

        public LivroConstrutor custa(final String custo) {
            Objects.requireNonNull(custo);
            this.custo = new BigDecimal(custo);
            return this;
        }

        public Livro construir() {
            return new Livro(this);
        }

    }

    private Livro(final LivroConstrutor livroConstrutor) {
        id = livroConstrutor.id;
        nome = livroConstrutor.nome;
        dataLancamento = livroConstrutor.dataLancamento;
        custo = livroConstrutor.custo;
    }

    private final long id;

    private final String nome;

    private final LocalDate dataLancamento;

    private final BigDecimal custo;

    public long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public LocalDate getDataLancamento() {
        return dataLancamento;
    }

    public BigDecimal getCusto() {
        return custo;
    }

    boolean anoAtual(final LocalDate data) {
        return getDataLancamento().getYear() == data.getYear();
    }

}
