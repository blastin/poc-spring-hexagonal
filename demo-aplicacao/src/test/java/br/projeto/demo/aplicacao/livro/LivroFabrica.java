package br.projeto.demo.aplicacao.livro;

import java.time.LocalDate;
import java.util.SplittableRandom;

public class LivroFabrica {

    public static Livro fabricar() {
        return fabricar("Livro Fabrica");
    }

    public static Livro fabricar(final String nomeLivro) {
        return fabricar(nomeLivro, FormatarData.formatarData(LocalDate.now()));
    }

    public static Livro fabricar(final String nomeLivro, final String dataLancamento) {

        return
                Livro
                        .builder()
                        .comId(new SplittableRandom().nextLong(1, 100))
                        .custa("376.00")
                        .seuNome(nomeLivro)
                        .lancamentoOcorreu(dataLancamento)
                        .construir();

    }

    private LivroFabrica() {
    }

}

