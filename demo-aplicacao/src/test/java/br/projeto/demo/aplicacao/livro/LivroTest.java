package br.projeto.demo.aplicacao.livro;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;

class LivroTest {

    @Test
    void construirLivro() {

        final String dataLancamento = "28/01/2020";

        final String nomeLivro = "Fundamentals of Software Architecture: An Engineering Approach";

        final Livro livro = LivroFabrica.fabricar(nomeLivro, dataLancamento);

        Assertions
                .assertTrue(livro.getId() > 0);

        Assertions
                .assertEquals(BigDecimal.valueOf(376).setScale(2, RoundingMode.CEILING), livro.getCusto());

        Assertions
                .assertEquals(nomeLivro, livro.getNome());

        Assertions
                .assertEquals(FormatarData.localDate(dataLancamento), livro.getDataLancamento());

    }

    @Test
    void livroComAnoAtual() {

        final Livro livro = LivroFabrica.fabricar();

        Assertions
                .assertTrue(livro.anoAtual(LocalDate.now()));

    }

    @Test
    void livroNaoEhDoAnoAtual() {

        final Livro livro = LivroFabrica
                .fabricar
                        ("O Mítico homem-mês",
                                FormatarData.formatarData(LocalDate.of(1975, 2, 8))
                        );

        Assertions
                .assertFalse(livro.anoAtual(LocalDate.now()));

    }

    @Test
    void idMenorQueZero() {

        Assertions
                .assertThrows(IllegalArgumentException.class, () -> Livro.builder().comId(-1));

    }

    @Test
    void nomeNulo() {

        Assertions
                .assertThrows(NullPointerException.class, () -> Livro.builder().seuNome(null));

    }

    @Test
    void dataLancamentoComPadraoNaoReconhecida() {

        Assertions
                .assertThrows(DateTimeParseException.class, () -> Livro.builder().lancamentoOcorreu("null"));

    }

    @Test
    void valorDeCustoNulo() {

        Assertions
                .assertThrows(NullPointerException.class, () -> Livro.builder().custa(null));

    }

}