package br.projeto.demo.aplicacao.livro.portas;

import br.projeto.demo.aplicacao.livro.CatalogarLivrosRecentes;
import br.projeto.demo.aplicacao.livro.Livro;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static br.projeto.demo.aplicacao.livro.LivroFabrica.fabricar;

class CasoDeUsoCatalogarLivrosRecentesTest {

    @Test
    void recuperarLivrosRecentes() {

        final List<Livro> livros =
                List
                        .of(
                                fabricar("Arquitetura Limpa"),
                                fabricar("Java Efetivo 3 ed PT-BR", "03/02/2019"));

        final PortaRecuperarLivros portaRecuperarLivros =
                () -> livros
                        .stream()
                        .collect(Collectors.toUnmodifiableSet());

        final CasoDeUsoCatalogarLivrosRecentes casoDeUsoCatalogarLivrosRecentes =
                new CatalogarLivrosRecentes(portaRecuperarLivros);

        final Set<Livro> livrosRecentes = casoDeUsoCatalogarLivrosRecentes.catalogar();

        Assertions
                .assertNotNull(livrosRecentes);

        Assertions
                .assertFalse(livrosRecentes.isEmpty());

        Assertions
                .assertEquals(1, livrosRecentes.size());

        Assertions
                .assertEquals(livros.get(0), livrosRecentes.iterator().next());

    }
}