# Prova de conceito para spring em módulos

## Como rodar ? (recomendado) 

```shell script
docker run -it --rm -p 8080:8080 -e AMBIENTE_APLICACAO=dev registry.gitlab.com/blastin/poc-spring-hexagonal:main
```

## Como buildar / rodar o projeto ?

#### docker

```shell script
git clone https://gitlab.com/blastin/poc-spring-hexagonal.git
cd poc-spring-hexagonal
docker build -t demo -f build-docker/Dockerfile .
docker run -it --rm -p 8080:8080 -e AMBIENTE_APLICACAO=dev demo
```

#### JDK 11

```shell script
git clone https://gitlab.com/blastin/poc-spring-hexagonal.git 
cd poc-spring-hexagonal
chmod +x mvnw
./mvnw clean compile package
java -jar demo-configuracao/target/demo-configuracao-0.0.1-SNAPSHOT.jar
```

---

### Como verificar se está funcionando ? 

#### navegador

Acesse pelo seu navegador favorito : [localhost:8080/livros](http://localhost:8080/livros)

#### shell script

```shell script
curl -H "Accept: application/json" 'localhost:8080/livros' | json_pp
```

#### Qual saída esperada ?

```json
[
   {
      "id" : 0,
      "nome" : "POC 2.0"
   }
]
```

```java
class Atencao {
    public static void main(String[] args){
        System.out.println("O valor de id pode variar");
    }
}
```

## Como os módulos estão organizados ? (árvore hierárquica)

    0 demo (pom)
        1 demo-adaptadores (pom)
            2 demo-adaptador-persistencia (compile)
                src/main/java
            2 demo-adaptador-web (compile)
                src/main/java
        1 demo-aplicacao (compile)
            src/main/java
        1 demo-configuracao (compile)
            src/main/java

### Qual framework o projeto utiliza ?

```xml
    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.3.3.RELEASE</version>
        <relativePath/>
    </parent>
```

### Tabela de dependências

Nome | Versão
----- | -----
h2 | 1.4.200
dependency-check-maven | 5.3.2
spring-boot-starter-test | 2.3.3.RELEASE
spring-boot-starter-web | ~
spring-boot-starter-data-jpa | ~
spring-boot-devtools | ~
spring-boot-maven-plugin | ~
