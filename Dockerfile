FROM lisboajeff/openjdk11-jre:alpine

COPY demo-configuracao/target/*.jar aplicativo.jar

CMD ["java", "-jar", "-Dspring.profiles.active=${AMBIENTE_APLICACAO}", "aplicativo.jar"]
