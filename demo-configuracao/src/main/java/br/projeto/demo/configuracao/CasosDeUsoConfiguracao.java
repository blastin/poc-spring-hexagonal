package br.projeto.demo.configuracao;

import br.projeto.demo.aplicacao.livro.CatalogarLivrosRecentes;
import br.projeto.demo.aplicacao.livro.portas.CasoDeUsoCatalogarLivrosRecentes;
import br.projeto.demo.aplicacao.livro.portas.PortaRecuperarLivros;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CasosDeUsoConfiguracao {

    @Bean
    CasoDeUsoCatalogarLivrosRecentes casoDeUsoCatalogarLivrosRecentes(final PortaRecuperarLivros porta) {
        return new CatalogarLivrosRecentes(porta);
    }

}
