package br.projeto.demo.configuracao;

import br.projeto.demo.web.Controlador;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses = Controlador.class)
public class ControladoresConfiguracao {
}
