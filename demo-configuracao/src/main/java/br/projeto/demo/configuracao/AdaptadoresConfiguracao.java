package br.projeto.demo.configuracao;

import br.projeto.demo.aplicacao.livro.FormatarData;
import br.projeto.demo.persistencia.livro.AdaptadorRecuperarLivros;
import br.projeto.demo.persistencia.livro.EntidadeLivro;
import br.projeto.demo.persistencia.livro.RepositorioLivro;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.time.LocalDate;
import java.util.SplittableRandom;

@Configuration
@EntityScan(basePackages = "br.projeto.demo.persistencia")
@EnableJpaRepositories(basePackages = "br.projeto.demo.persistencia")
public class AdaptadoresConfiguracao {

    @Bean
    AdaptadorRecuperarLivros adaptadorRecuperarLivros(final RepositorioLivro repositorioLivro) {

        final EntidadeLivro entidadeLivro = new EntidadeLivro();

        entidadeLivro.setCusto("200");
        entidadeLivro.setDataLancamento(FormatarData.formatarData(LocalDate.now()));
        entidadeLivro.setId(new SplittableRandom().nextLong(10, 1000));
        entidadeLivro.setNome("POC 2.0");

        repositorioLivro.save(entidadeLivro);

        return new AdaptadorRecuperarLivros(repositorioLivro);

    }

}
