module br.projeto.demo.persistencia {
    requires br.projeto.demo.aplicacao;
    requires spring.data.jpa;
    requires java.persistence;
}