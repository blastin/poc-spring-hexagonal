package br.projeto.demo.persistencia.livro;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RepositorioLivro extends JpaRepository<EntidadeLivro, Long> {
}
