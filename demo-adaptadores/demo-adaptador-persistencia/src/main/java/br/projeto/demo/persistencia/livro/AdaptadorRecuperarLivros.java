package br.projeto.demo.persistencia.livro;

import br.projeto.demo.aplicacao.livro.Livro;
import br.projeto.demo.aplicacao.livro.portas.PortaRecuperarLivros;

import java.util.Set;
import java.util.stream.Collectors;

public final class AdaptadorRecuperarLivros implements PortaRecuperarLivros {

    public AdaptadorRecuperarLivros(final RepositorioLivro repositorioLivro) {
        this.repositorioLivro = repositorioLivro;
    }

    private final RepositorioLivro repositorioLivro;

    @Override
    public Set<Livro> recuperar() {
        return
                repositorioLivro
                        .findAll()
                        .stream()
                        .map(EntidadeLivro.LivroMapper::de)
                        .collect(Collectors.toUnmodifiableSet());
    }

}
