package br.projeto.demo.persistencia.livro;

import br.projeto.demo.aplicacao.livro.Livro;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class EntidadeLivro {

    private Long id;

    private String nome;

    private String dataLancamento;

    private String custo;

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDataLancamento() {
        return dataLancamento;
    }

    public void setDataLancamento(String dataLancamento) {
        this.dataLancamento = dataLancamento;
    }

    public String getCusto() {
        return custo;
    }

    public void setCusto(String custo) {
        this.custo = custo;
    }

    static class LivroMapper {

        static Livro de(final EntidadeLivro entidadeLivro) {

            return
                    Livro
                            .builder()
                            .custa(entidadeLivro.getCusto())
                            .lancamentoOcorreu(entidadeLivro.getDataLancamento())
                            .seuNome(entidadeLivro.getNome())
                            .comId(entidadeLivro.getId())
                            .construir();

        }
    }

}
