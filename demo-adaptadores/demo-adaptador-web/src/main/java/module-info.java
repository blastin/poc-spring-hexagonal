module br.projeto.demo.web {

    requires spring.web;

    requires com.fasterxml.jackson.annotation;

    requires br.projeto.demo.aplicacao;

    opens br.projeto.demo.web;

}