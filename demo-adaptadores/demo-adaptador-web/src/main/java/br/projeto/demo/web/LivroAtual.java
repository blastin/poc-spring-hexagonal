package br.projeto.demo.web;

import br.projeto.demo.aplicacao.livro.Livro;
import com.fasterxml.jackson.annotation.JsonProperty;

final class LivroAtual {

    LivroAtual(final Livro livro) {
        this.livro = livro;
    }

    /**
     * Jackson apenas serializa pelos métodos getter
     */
    private final Livro livro;

    @JsonProperty("id-livro")
    public long getId() {
        return livro.getId();
    }

    @JsonProperty("nome-livro")
    public String getNome() {
        return livro.getNome();
    }

}
