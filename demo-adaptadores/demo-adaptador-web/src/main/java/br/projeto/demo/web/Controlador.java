package br.projeto.demo.web;

import br.projeto.demo.aplicacao.livro.portas.CasoDeUsoCatalogarLivrosRecentes;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("livros")
public class Controlador {

    Controlador(final CasoDeUsoCatalogarLivrosRecentes casoDeUsoCatalogarLivrosRecentes) {
        this.casoDeUsoCatalogarLivrosRecentes = casoDeUsoCatalogarLivrosRecentes;
    }

    final CasoDeUsoCatalogarLivrosRecentes casoDeUsoCatalogarLivrosRecentes;

    @GetMapping(produces = "application/json")
    public Set<LivroAtual> livrosAtuais() {
        return
                casoDeUsoCatalogarLivrosRecentes
                        .catalogar()
                        .stream()
                        .map(LivroAtual::new)
                        .collect(Collectors.toUnmodifiableSet());
    }

}
