package br.projeto.demo.web;

import br.projeto.demo.aplicacao.livro.portas.CasoDeUsoCatalogarLivrosRecentes;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Set;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ContextConfiguration(classes = {Controlador.class})
@WebMvcTest
@ActiveProfiles("test")
class ControladorTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CasoDeUsoCatalogarLivrosRecentes casoDeUsoCatalogarLivrosRecentes;

    @Test
    public void deveRetornarUmArrayDeLivrosAtuais() throws Exception {

        when(casoDeUsoCatalogarLivrosRecentes.catalogar())
                .thenReturn(Set.of(LivroWebFabrica.fabricar()));

        mockMvc
                .perform(
                        get("/livros")
                                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("[{\"id-livro\":1,\"nome-livro\":\"POC 2.0 Test\"}]"));

    }

}