package br.projeto.demo.web;

import br.projeto.demo.aplicacao.livro.Livro;

public class LivroWebFabrica {

    public static Livro fabricar() {
        return Livro.builder().comId(1L).seuNome("POC 2.0 Test").construir();
    }

    private LivroWebFabrica() {
    }

}
